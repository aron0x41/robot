from pydantic import BaseModel


class Wheels(BaseModel):
    left: int  # -100 to 100
    right: int  # -100 to 100
