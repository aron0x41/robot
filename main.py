import random
import string

from fastapi import Cookie, FastAPI, Request, Response, status
from fastapi.staticfiles import StaticFiles
from fastapi.templating import Jinja2Templates

import motor
from models import Wheels


app = FastAPI(openapi_url=None)
app.mount("/static", StaticFiles(directory="static"), name="static")
templates = Jinja2Templates(directory="templates")

token = "".join(random.choices(string.ascii_uppercase + string.digits, k=6))
print(f"TOKEN: {token}")


@app.on_event("startup")
async def startup_event():
    motor.init()


@app.on_event("shutdown")
async def shutdown_event():
    motor.exit()


@app.get("/")
async def index(request: Request):
    return templates.TemplateResponse(
        "index.html",
        {
            "request": request
        }
    )


@app.get("/manually")
async def manually(request: Request):
    return templates.TemplateResponse(
        "manually.html",
        {
            "request": request
        }
    )


@app.get("/automated")
async def automated(request: Request):
    return templates.TemplateResponse(
        "automated.html",
        {
            "request": request
        }
    )


@app.post("/wheels")
async def set_wheels(wheels: Wheels, t: str = Cookie("")):
    if t.upper() != token:
        return Response(status_code=status.HTTP_401_UNAUTHORIZED)
    if not (-100 <= wheels.left <= 100 and -100 <= wheels.right <= 100):
        return Response(status_code=status.HTTP_400_BAD_REQUEST)
    print(wheels)
    motor.left_wheel(wheels.left)
    motor.right_wheel(wheels.right)
    return Response(status_code=status.HTTP_204_NO_CONTENT)
