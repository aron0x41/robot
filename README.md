# Robot

Visit the project on [GitLab](https://gitlab.com/aron0x41/robot)

## Installation

```bash
apt install python3 python3-pip git
git clone https://gitlab.com/aron0x41/robot.git
cd robot
cp example_config.yml config.yml
nano config.yml  # Edit config
python3 -m pip install -r requirements.txt
```

## Usage

Port 80 requires root access

```bash
python3 -m uvicorn main:app --host 0.0.0.0 --port 80
```

## Update

```bash
git pull
python3 -m pip install -r requirements.txt
```

## Pins

![Raspberry Pi 3 Pins](/assets/pi_pins.png)

![L293D](/assets/L293D_pins.png)

![H-Bridge](/assets/h-bridge.png)
