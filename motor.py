import RPi.GPIO as GPIO


from config import config


GPIO.setmode(GPIO.BCM)

GPIO.setup(config['gpio_pins']['input1'], GPIO.OUT)
GPIO.setup(config['gpio_pins']['input2'], GPIO.OUT)
GPIO.setup(config['gpio_pins']['input3'], GPIO.OUT)
GPIO.setup(config['gpio_pins']['input4'], GPIO.OUT)


pwm_left = GPIO.PWM(config['gpio_pins']['input1'], 100)
pwm_right = GPIO.PWM(config['gpio_pins']['input4'], 100)


def left_wheel(value: int):
    if value > 0:
        GPIO.output(config['gpio_pins']['input1'], GPIO.LOW)
        GPIO.output(config['gpio_pins']['input2'], GPIO.HIGH)
        pwm_left.ChangeDutyCycle(value)
    elif value < 0:
        GPIO.output(config['gpio_pins']['input1'], GPIO.HIGH)
        GPIO.output(config['gpio_pins']['input2'], GPIO.LOW)
        pwm_left.ChangeDutyCycle(-value)
    else:
        GPIO.output(config['gpio_pins']['input1'], GPIO.LOW)
        GPIO.output(config['gpio_pins']['input2'], GPIO.LOW)
        pwm_left.ChangeDutyCycle(0)


def right_wheel(value: int):
    if value > 0:
        GPIO.output(config['gpio_pins']['input3'], GPIO.LOW)
        GPIO.output(config['gpio_pins']['input4'], GPIO.HIGH)
        pwm_right.ChangeDutyCycle(value)
    elif value < 0:
        GPIO.output(config['gpio_pins']['input3'], GPIO.HIGH)
        GPIO.output(config['gpio_pins']['input4'], GPIO.LOW)
        pwm_right.ChangeDutyCycle(-value)
    else:
        GPIO.output(config['gpio_pins']['input3'], GPIO.LOW)
        GPIO.output(config['gpio_pins']['input4'], GPIO.LOW)
        pwm_right.ChangeDutyCycle(0)


def init():
    GPIO.output(config['gpio_pins']['input1'], GPIO.LOW)
    GPIO.output(config['gpio_pins']['input2'], GPIO.LOW)
    GPIO.output(config['gpio_pins']['input3'], GPIO.LOW)
    GPIO.output(config['gpio_pins']['input4'], GPIO.LOW)
    pwm_left.start(0)
    pwm_right.start(0)


def exit():
    pwm_left.stop()
    pwm_right.stop()
    GPIO.cleanup()
