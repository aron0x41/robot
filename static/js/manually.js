var keys = {
    "w": false,
    "a": false,
    "s": false,
    "d": false
}


function update_wheels() {
    var vector = [0, 0];
    if (keys["w"]) {
        vector[1] += 1;
    }
    if (keys["s"]) {
        vector[1] -= 1;
    }
    if (keys["a"]) {
        vector[0] -= 1;
    }
    if (keys["d"]) {
        vector[0] += 1;
    }

    var speed = document.getElementById("speed").value;
    if (vector[0] == 0) {  // forward or backward
        var left = vector[1] * speed;
        var right = vector[1] * speed;
    } else if (vector[1] == 0) {  // left or right
        var left = vector[0] * 100;
        var right = -vector[0] * 100;
    } else {  // diagonal
        if (vector[0] > 0) {
            var left = vector[1] * speed;
            var right = Math.floor(vector[1] * speed / 2);
        } else {
            var left = Math.floor(vector[1] * speed / 2);
            var right = vector[1] * speed;
        }
    }

    fetch("/wheels", {
        method: "POST",
        headers: {
            "Content-Type": "application/json"
        },
        body: JSON.stringify({
            "left": left,
            "right": right
        })
    }).then(response => {
        if (response.ok) {
            if (left > 0) {
                document.getElementsByClassName("wheel_part")[0].style.background = `linear-gradient(transparent 0%, transparent ${100 - left}%, green ${100 - left}%, green 100%)`;
                document.getElementsByClassName("wheel_part")[1].style.background = `transparent`;
            } else {
                document.getElementsByClassName("wheel_part")[0].style.background = `transparent`;
                document.getElementsByClassName("wheel_part")[1].style.background = `linear-gradient(red 0%, red ${-left}%, transparent ${-left}%, transparent 100%)`;
            }
            if (right > 0) {
                document.getElementsByClassName("wheel_part")[2].style.background = `linear-gradient(transparent 0%, transparent ${100 - right}%, green ${100 - right}%, green 100%)`;
                document.getElementsByClassName("wheel_part")[3].style.background = `transparent`;
            } else {
                document.getElementsByClassName("wheel_part")[2].style.background = `transparent`;
                document.getElementsByClassName("wheel_part")[3].style.background = `linear-gradient(red 0%, red ${-right}%, transparent ${-right}%, transparent 100%)`;
            }
        } else if (response.status == 401) {
            document.cookie = `t=${prompt("Enter token")}`;
        } else {
            alert(`Error: ${response.status}`);
        }
    });
}


function keyevent(event) {
    if (event.repeat) {
        return;
    }
    if (event.key in keys) {
        keys[event.key] = event.type == "keydown";
        update_wheels();
    }
}


function init() {
    document.addEventListener("keydown", keyevent);
    document.addEventListener("keyup", keyevent);
}
